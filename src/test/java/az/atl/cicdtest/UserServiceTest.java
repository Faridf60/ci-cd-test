package az.atl.cicdtest;

import az.atl.cicdtest.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = UserService.class)
class UserServiceTest {

    @Autowired
    UserService service;

    @Test
    void getUser() {
        Assertions.assertEquals("Mamed", service.getUser().getName());
    }
}