package az.atl.cicdtest.dto;

import static lombok.AccessLevel.PRIVATE;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = PRIVATE)
@AllArgsConstructor
public class UserResponse {

    String name;
    Integer age;
}
