package az.atl.cicdtest.service;

import az.atl.cicdtest.dto.UserResponse;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public UserResponse getUser() {
        return new UserResponse("Mamed", 39);
    }
}
