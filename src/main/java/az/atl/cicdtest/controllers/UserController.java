package az.atl.cicdtest.controllers;

import az.atl.cicdtest.dto.UserResponse;
import az.atl.cicdtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @GetMapping
    public UserResponse getUser() {
        return service.getUser();
    }
}
